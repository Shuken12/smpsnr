# Smpsnr

"Smpsnr" is a tutorial scenario of the historical war simulation game "Sengokushi".

# DEMO

![](https://wiki.sengokushi.net/_media/smp_v2.jpg)


# Features

The scenario has an overwhelming number of lords, castles and warlords.
The goal is to become the best japanese "Sengoku" era strategy simulation game in the world.

# Requirement

* Sengokushi 1.16d
* default_q.smp

# Installation

Download to any folder

# Usage

Select the scenario file with "Sengokushi"

# Note

If you do not purchase the Sengokushi license key, you can only play for 2 years in the scenario time.

# Author

* FATE
* ころん
* 槍
* 播磨
* はんみょう
* 流水亭
* 鉱山担当
* Cone
* 守宮
* ガラクトース
* 御神楽
* 神楽
* 初心
* 諸芯
* AJI
* 金海
* 藩主
* 事後
* 深夜
* ごう
* MMS
* any more...

# Advicer and Debugger

* chachahoo
* ガラクトース
* ツェッペリン
* Ein
* ＿＿＿
* itakura
* χι
* 時計
* 軍略家
* 将軍家再興
* hylux
* 酢酸カーミン
* chikaru
* 北
* ととろアリス
* くろねこ
* 宇喜多
* weed
* Ｔｏｋｏｓｕ
* Maton
* 投降者
* July of Main
* 秦泉寺
* B級職人
* 小ート
* any more...

# License

"Smpsnr" is under [MIT license](https://en.wikipedia.org/wiki/MIT_License).
